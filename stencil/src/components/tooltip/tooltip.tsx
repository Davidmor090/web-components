import { Component, h, Prop } from "@stencil/core";

@Component({
  tag: "uc-stencil-tooltip",
  styleUrl: "./tooltip.css",
  shadow: true 
})
export class Tooltip {
  @Prop({reflectToAttr: true}) content: string;
  @Prop({reflectToAttr: true, mutable: true}) opened = false;

  toogleTooltip() {
    this.opened = !this.opened;
  }

  render() {
    return [
      <slot>Some default</slot>,
      <span onClick={this.toogleTooltip.bind(this)} class="icon">?</span>,
      <div>{this.content}</div>
    ];
  }
}