import { Component, h, Prop, State, Method } from "@stencil/core";

@Component({
  tag: "uc-side-drawer",
  styleUrl: "./side-drawer.css",
  shadow: true 
})
export class SideDrawer {
  @State() showContactInfo = false;
  @Prop({reflectToAttr: true}) title: string;
  @Prop({reflectToAttr: true, mutable: true}) opened: boolean;

  onCloseDrawer() {
    this.opened = false;
  }

  onContentChange(content: string) {
    this.showContactInfo = content === "contact";
  }

  @Method()
  async open() {
    this.opened = true;
  }

  render() {
    let mainContent = <slot />;
    if (this.showContactInfo) {
      mainContent = (
        <div id="contact-information">
          <h2>Contact Information</h2>
          <p>You can reach us with a phone or mail</p>
          <ul>
            <li>Phone: 2355243545</li>
            <li>Mail: <a href="mailto:something@something.com">something@something.com</a></li>
          </ul>
        </div>
      );
    }
   

    return [
      <div onClick={this.onCloseDrawer.bind(this)} class="backdrop"></div>,
      <aside>
        <header>
          <h1>{this.title}</h1>
          <button onClick={this.onCloseDrawer.bind(this)}>X</button>
        </header>
        <section id="tabs">
          <button onClick={this.onContentChange.bind(this, "nav")} class={!this.showContactInfo && "active"}>Navigation</button>
          <button onClick={this.onContentChange.bind(this, "contact")} class={this.showContactInfo && "active"}>Contact</button>
        </section>
        <main>
          {mainContent}
        </main>
      </aside>
    ];
  }
}